package com.previewscan;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import com.services.ImageViewInitService;
import com.services.ScanButtonViewInitService;
import com.services.TitleViewInitService;

public class ReportActivity extends AppCompatActivity {

    public static final String START_MESSAGE = "Please scan a barcode";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.report_layout);
    }
}
