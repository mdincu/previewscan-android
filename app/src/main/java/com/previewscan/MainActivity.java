package com.previewscan;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.services.ImageViewInitService;
import com.services.ScanButtonViewInitService;
import com.services.TitleViewInitService;
import com.tasks.BarcodeRetrieverTask;

public class MainActivity extends AppCompatActivity {

    public static final String START_MESSAGE = "Please scan a barcode";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_layout);

        TitleViewInitService.initBigTitle(this, START_MESSAGE);
        ImageViewInitService.initStartImage(this);
        ScanButtonViewInitService.initScanButton(this, R.id.big_scan);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        IntentResult scanningResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
        if (scanningResult.getFormatName() == null) {
            return;
        }
        if (scanningResult != null) {
            String barcode = scanningResult.getContents();
            new BarcodeRetrieverTask(this).execute(barcode);
        } else {
            Toast.makeText(getApplicationContext(), "No scan data received!", Toast.LENGTH_SHORT).show();
        }
    }
}
