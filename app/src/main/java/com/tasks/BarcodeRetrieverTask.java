package com.tasks;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.StrictMode;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.previewscan.R;
import com.services.*;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class BarcodeRetrieverTask extends AsyncTask<String, Void, JSONObject> {

    public static final String PRVIEWSCAN_END_POINT = "http://192.168.1.113/rest/controller/barcode";
    public static final String BARCODE_PARAMETER = "barcode";
    public static final String NULL_RESPONE = "null";

    private final Activity activity;
    private ProgressDialog dialog;

    public BarcodeRetrieverTask(Activity activity) {
        this.activity = activity;
        this.dialog = ProgressDialog.show(activity, "Searching", "Please wait...");
    }

    @Override
    protected JSONObject doInBackground(String... params) {
        String barcode = params[0];
        try {
            JSONObject response = new JSONObject();
            response.put("barcode", barcode);
            response.put("product", getPostResponse(PRVIEWSCAN_END_POINT, BARCODE_PARAMETER, barcode));
            return response;
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String getPostResponse(String url, String parameterName, String parameterValue) throws IOException {
        HttpPost httpPost = new HttpPost(url);
        List<NameValuePair> postParameters = new ArrayList<>();
        postParameters.add(new BasicNameValuePair(parameterName, parameterValue));
        httpPost.setEntity(new UrlEncodedFormEntity(postParameters));
        HttpClient httpClient = new DefaultHttpClient();
        HttpResponse httpResponse = httpClient.execute(httpPost);
        HttpEntity httpEntity = httpResponse.getEntity();
        return EntityUtils.toString(httpEntity);
    }

    @Override
    protected void onPostExecute(JSONObject response) {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        if (null != response && response.has("product")) {
            ProductFoundService.initProductFound(activity, response);
        } else {
            ProductNotFoundService.initProductNotFound(activity);
        }
        dialog.dismiss();
    }

}
