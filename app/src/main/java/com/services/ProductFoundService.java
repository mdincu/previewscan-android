package com.services;

import android.app.Activity;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ProductFoundService {

    public static final String POSTS = "posts";

    public static void initProductFound(final Activity activity, final JSONObject response) {
        try {
            String barcode = response.getString("barcode");
            JSONObject product = new JSONObject(response.getString("product"));
            final JSONArray reviews = product.has(POSTS) ? product.getJSONArray(POSTS) : null;
            if (reviews != null && reviews.length() > 0) {
                ProductWithReviewService.initViewWithReviews(activity, product, barcode, reviews);
            } else {
                ProductWithoutReviewService.initViewWithoutReviews(activity, product, barcode);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
