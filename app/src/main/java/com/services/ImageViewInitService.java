package com.services;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.widget.ImageView;
import android.widget.TextView;
import com.previewscan.R;
import org.json.JSONObject;

import java.net.URL;

public class ImageViewInitService {

    public static void initImage(final JSONObject jsonObject, final Activity activity, int id) {
        final ImageView imageView = (ImageView) activity.findViewById(id);
        try {
            final String imageUrl = jsonObject.has("images") ? jsonObject.getJSONArray("images").getJSONObject(0).getString("url") : null;
            final URL url = new URL(imageUrl);
            final Bitmap image = BitmapFactory.decodeStream(url.openConnection().getInputStream());
            imageView.setImageBitmap(image);
        } catch (Exception e) {
            e.printStackTrace();
            imageView.setImageResource(R.drawable.noimagefound);
        }
    }

    public static void initImageNotFound(final Activity activity, int id) {
        final ImageView imageView = (ImageView) activity.findViewById(id);
        imageView.setImageResource(R.drawable.noimagefound);
    }

    public static void initStartImage(final Activity activity){
        final ImageView startImage = (ImageView) activity.findViewById(R.id.big_image);
        startImage.setImageResource(R.drawable.newscan);
    }
}
