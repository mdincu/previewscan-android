package com.services;

import android.app.Activity;
import com.previewscan.R;

public class ProductNotFoundService {

    public static final String ERROR_MESSAGE = "Barcode has not returned any result!";

    public static void initProductNotFound(final Activity activity) {
        activity.setContentView(R.layout.main_layout);
        ReportViewInitService.initReportButton(activity, "", R.id.main, R.id.big_title);
        TitleViewInitService.initBigTitle(activity, ERROR_MESSAGE);
        ImageViewInitService.initImageNotFound(activity, R.id.big_image);
        ScanButtonViewInitService.initScanButton(activity, R.id.big_scan);
    }
}
