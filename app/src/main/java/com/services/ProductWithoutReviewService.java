package com.services;

import android.app.Activity;
import com.previewscan.R;
import org.json.JSONObject;

public class ProductWithoutReviewService {

    public static void initViewWithoutReviews(Activity activity, JSONObject jsonObject, String barcode) {
        activity.setContentView(R.layout.main_layout);
        ReportViewInitService.initReportButton(activity, barcode, R.id.main, R.id.big_title);
        TitleViewInitService.initSmallTitle(jsonObject, activity, R.id.big_title);
        ImageViewInitService.initImage(jsonObject, activity, R.id.big_image);
        ScanButtonViewInitService.initScanButton(activity, R.id.big_scan);
    }
}
