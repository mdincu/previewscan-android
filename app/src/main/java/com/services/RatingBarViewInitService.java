package com.services;

import android.app.Activity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.RatingBar;
import com.previewscan.R;
import org.json.JSONException;
import org.json.JSONObject;

public class RatingBarViewInitService {

    public static final String AVERAGE_RATING = "averageRating";

    public static void initRatingBar(final JSONObject jsonObject, final Activity activity) {
        final RatingBar ratingBar = (RatingBar) activity.findViewById(R.id.item_rating);
        ratingBar.setStepSize(0.1f);
        ratingBar.setRating(3.4f);
        if(jsonObject.has(AVERAGE_RATING)){
            float averageRating = 0;
            try {
                averageRating = (float) jsonObject.getDouble(AVERAGE_RATING);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            ratingBar.setRating(averageRating);
        }


        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                if(fromUser) {
                    ratingBar.setStepSize(1.0f);
                    //ratingBar.setRating(rating);
                    //UpdateRatingService.update(rating);
                }
            }
        });

        ratingBar.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                ratingBar.setStepSize(1.0f);
                return true;
            }
        });
    }
}
