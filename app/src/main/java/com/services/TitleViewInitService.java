package com.services;

import android.app.Activity;
import android.widget.TextView;
import com.previewscan.R;
import org.json.JSONObject;

public class TitleViewInitService {

    public static final String TITLE_ATTRIBUTE = "title";
    public static final String TITLE_NOT_FOUND = "No title was found!";

    public static void initBigTitle(final Activity activity, final String message){
        ((TextView) activity.findViewById(R.id.big_title)).setText(message);
    }

    public static void initSmallTitle(final JSONObject jsonObject, final Activity activity, final int id) {
        try {
            final String itemTitle = (String) jsonObject.get(TITLE_ATTRIBUTE);
            ((TextView) activity.findViewById(id)).setText(itemTitle);
        } catch (Exception e) {
            e.printStackTrace();
            ((TextView) activity.findViewById(id)).setText(TITLE_NOT_FOUND);
        }
    }
}
