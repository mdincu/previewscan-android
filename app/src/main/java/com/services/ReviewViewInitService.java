package com.services;

import android.app.Activity;
import android.widget.ListView;
import com.adapters.ReviewAdapter;
import com.previewscan.R;
import org.json.JSONArray;

public class ReviewViewInitService {

    public static void initReviews(final JSONArray reviews, final Activity activity) {
        final ReviewAdapter adapter = new ReviewAdapter(activity, reviews);
        final ListView listView = (ListView) activity.findViewById(R.id.review_list);
        listView.setAdapter(adapter);
    }
}
