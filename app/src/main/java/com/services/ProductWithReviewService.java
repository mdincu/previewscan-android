package com.services;

import android.app.Activity;
import com.previewscan.R;
import org.json.JSONArray;
import org.json.JSONObject;

public class ProductWithReviewService {

    public static void initViewWithReviews(Activity activity, JSONObject jsonObject, String barcode, JSONArray reviews) {
        activity.setContentView(R.layout.result_with_reviews_layout);
        ReportViewInitService.initReportButton(activity, barcode, R.id.result, R.id.item_image_small);
        RatingBarViewInitService.initRatingBar(jsonObject, activity);
        TitleViewInitService.initSmallTitle(jsonObject, activity, R.id.item_title_small);
        ImageViewInitService.initImage(jsonObject, activity, R.id.item_image_small);
        ReviewViewInitService.initReviews(reviews, activity);
    }
}
