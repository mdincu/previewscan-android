package com.services;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.previewscan.R;
import com.previewscan.ReportActivity;

public class ReportViewInitService {
    public static void initReportButton(Activity activity, String barcode, int layoutId, int relativeId) {
        RelativeLayout relativeLayout = (RelativeLayout) activity.findViewById(layoutId);
        View reportButton = createReportButton(activity, barcode, relativeId);
        relativeLayout.addView(reportButton);
    }

    private static View createReportButton(final Activity activity, String barcode, int relativeId){
        Button reportButton = new Button(activity);

        reportButton.setText(R.string.report);

        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.addRule(RelativeLayout.BELOW, relativeId);
        params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        reportButton.setLayoutParams(params);

        reportButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(activity, ReportActivity.class);
                activity.startActivity(intent);
            }
        });

        return reportButton;
    }
}
