package com.services;

import android.app.Activity;
import android.view.View;
import android.widget.Button;
import com.google.zxing.integration.android.IntentIntegrator;

public class ScanButtonViewInitService {

    public static void initScanButton(final Activity activity, final int id) {
        final Button scanButton = (Button) activity.findViewById(id);
        scanButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v.getId() == id) {
                    IntentIntegrator scanIntegrator = new IntentIntegrator(activity);
                    scanIntegrator.initiateScan();
                }
            }
        });
    }
}
