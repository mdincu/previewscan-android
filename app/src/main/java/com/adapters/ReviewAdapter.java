package com.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.previewscan.R;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ReviewAdapter extends BaseAdapter{

    private Activity activity;
    private JSONArray reviews;
    private LayoutInflater inflater;

    public ReviewAdapter(Activity activity, JSONArray reviews){
        this.activity = activity;
        this.reviews = reviews;
        this.inflater = (LayoutInflater) this.activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return reviews.length();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null) { convertView = inflater.inflate(R.layout.review_container_layout, null); }
        try {

            JSONObject review = reviews.getJSONObject(position);

            LinearLayout reviewLayout = (LinearLayout) convertView.findViewById(R.id.review_container);

            LinearLayout reviewElement = createReviewElement(convertView);

            TextView author = new TextView(convertView.getContext());
            author.setText(review.getString("author"));
            reviewElement.addView(author);

            TextView date = new TextView(convertView.getContext());
            date.setText(review.getString("date"));
            reviewElement.addView(date);

            TextView comment = new TextView(convertView.getContext());
            comment.setText(review.getString("text"));
            reviewElement.addView(comment);

            reviewLayout.addView(reviewElement);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return convertView;
    }

    private LinearLayout createReviewElement(View convertView) {
        LinearLayout reviewElement = createNewLinearLayout(convertView.getContext());
        reviewElement.setLayoutParams(createNewLayoutParams(10, 10, 10, 10));
        reviewElement.setPadding(10,5,10,5);
        GradientDrawable reviewElementGradient = new GradientDrawable();
        reviewElementGradient.setCornerRadius(12);
        reviewElementGradient.setColor(Color.rgb(226, 237, 255));
        reviewElement.setBackgroundDrawable(reviewElementGradient);
        return reviewElement;
    }

    private LinearLayout createNewLinearLayout(Context context){
        LinearLayout linearLayout = new LinearLayout(context);
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        return linearLayout;
    }

    private LinearLayout.LayoutParams createNewLayoutParams(int left, int top, int right, int bottom){
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(left, top, right, bottom);
        return layoutParams;
    }
}
